package se.sjostric.samples.parallel.build;

import static org.junit.Assert.*;

import org.junit.Test;

public class BuildResultTest {

	@Test
	public void test() {
		BuildResult buildResult = new BuildResult("id1");
		assertEquals("id1", buildResult.getBuildId());
	}

}
