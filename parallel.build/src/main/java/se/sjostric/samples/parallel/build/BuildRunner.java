package se.sjostric.samples.parallel.build;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class BuildRunner {

	public static void main(String[] args) {
		// Increasing numThreads should make this code scale upto number of CPU
		// cores, compare using the printed exec time
		int numThreads = 2;
		// atleast 10 should be set to keep the CPU busy and the result
		// predictable
		int numBuilds = 10;
		ExecutorService execService = Executors.newFixedThreadPool(numThreads);

		List<Future<BuildResult>> futureBuilds = new ArrayList<>();

		long startTime = System.currentTimeMillis();

		for (int i = 0; i < numBuilds; i++) {
			Future<BuildResult> futureBuild = execService
					.submit(new BuildExecutor(Integer.toString(i)));
			futureBuilds.add(futureBuild);
		}
		for (Future<BuildResult> future : futureBuilds) {
			try {
				BuildResult result = future.get();
				System.out.println("Result: " + result.toString());
			} catch (InterruptedException e) {
				e.printStackTrace();
				System.out.println("Thread was interrupted");
			} catch (ExecutionException e) {
				e.printStackTrace();
				System.out.println("Build failed");
			}

		}

		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;
		System.out.printf("Exectime %d ms\n", elapsedTime);

	}

}
