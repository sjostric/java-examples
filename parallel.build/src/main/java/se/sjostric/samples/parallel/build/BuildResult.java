package se.sjostric.samples.parallel.build;

public class BuildResult {

	private String buildId;
	private String log;
	private int sum;
	
	public BuildResult(String buildId) {
		this.buildId = buildId;
	}

	public enum Verdict {
		FAIL, SUCCESS
	}

	/** Build verdict */
	private Verdict buildVerdict;

	public Verdict getBuildVerdict() {
		return buildVerdict;
	}

	public void setBuildVerdict(Verdict buildVerdict) {
		this.buildVerdict = buildVerdict;
	}

	public String getBuildId() {
		return buildId;
	}

	public void setBuildId(String buildId) {
		this.buildId = buildId;
	}

	public String getLog() {
		return log;
	}

	public void setLog(String log) {
		this.log = log;
	}

	@Override
	public String toString() {
		return "Build: " + buildId + ", Verdict " + buildVerdict + ", sum: "
				+ sum;
	}

	public int getSum() {
		return sum;
	}

	public void setSum(int sum) {
		this.sum = sum;
	}

}
