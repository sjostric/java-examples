package se.sjostric.samples.parallel.build;

import java.util.concurrent.Callable;

public class BuildExecutor implements Callable<BuildResult> {

	String id;

	public BuildExecutor(String id) {
		this.id = id;
	}

	public BuildResult call() throws Exception {
		System.out.println("Executing build session: " + getId());
		// fake some execution, we cannot use sleep here since then the code
		// will scale even beyond number of CPU cores because the sleep will not
		// occupy the CPU
		int MAX = 100_000_000;
		int result = 0;
		for (int i = 0; i < MAX; i++) {
			result += i;
		}
		BuildResult buildResult = new BuildResult(getId());
		buildResult.setBuildVerdict(BuildResult.Verdict.SUCCESS);
		buildResult.setLog("Sum was: " + result);
		buildResult.setSum(result);
		return buildResult;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
